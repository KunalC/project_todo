ALTER TABLE tbl_todo_entries
ADD CONSTRAINT FK_tbl_todo_entries_tbl_users_todo
FOREIGN KEY (UserID)
REFERENCES tbl_users_todo(UserID)