<?php
    session_start();
    include 'functions.php';
    $session = sessionCheckout();
    if ($session){
        //Reading values from the form.
        $username = $_SESSION['username'];
        $_SESSION['url'] = "completeShow.php";
        $iUid = $_SESSION['uid'];
        $sStatus = "Completed";
        $mysqli = connectDB();
        if($mysqli){
            $resultCount = $mysqli->query("SELECT COUNT(UserID) FROM tbl_todo_entries WHERE UserID = '$iUid' and Status = '$sStatus'");
            //var_dump($resultCount);
            $rows = $resultCount->fetch_row();
            $iRecordCount = $rows[0];
            //var_dump($recordCount);
            $iRecordLimit = 3;
            // This tells us the page number of our last page
            $iLast = ceil($iRecordCount/$iRecordLimit);
            //var_dump($last);
            // This makes sure $last cannot be less than 1
            if($iLast < 1){
                $iLast = 1;
            }
            $iPageNum = 1;
            if(isset($_GET['pn'])){
                $iPageNum = preg_replace('#[^0-9]#', '', $_GET['pn']);
            }
            // This makes sure the page number isn't below 1, or more than our $last page
            if ($iPageNum < 1) {
                $iPageNum = 1;
            } else if ($iPageNum > $iLast) {
                $iPageNum = $iLast;
            }
            $limit = 'LIMIT ' .($iPageNum - 1) * $iRecordLimit .',' .$iRecordLimit;
            //var_dump($limit);
            //Retriving userid from tbl_users_todo.
            $Query = "SELECT * FROM tbl_todo_entries WHERE UserID = '$iUid' and Status = '$sStatus' $limit";
            //var_dump($Query);
            $result = $mysqli->query($Query);
            //$result = $mysqli->query("SELECT * FROM tbl_todo_entries WHERE UserID = \"$iUid\" and Status = \"$sStatus\"");
            //var_dump($result);
            if(!$result){
                echo "Error occured. (" . $mysqli->errno . ")" . $mysqli->error;
            }
            $vHeading = "Page <b>$iPageNum</b> of <b>$iLast</b>";
            $paginationCtrls = '';
            // If there is more than 1 page worth of results
            if($iLast != 1){
                /* First we check if we are on page one. If we are then we don't need a link to
                   the previous page or the first page so we do nothing. If we aren't then we
                   generate links to the first page, and to the previous page. */
                if ($iPageNum > 1) {
                    $previous = $iPageNum - 1;
                    $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'">Previous</a></li>';
                    // Render clickable number links that should appear on the left of the target page number
                    for($iCount = $iPageNum-4; $iCount < $iPageNum; $iCount++){
                        if($iCount > 0){
                            $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$iCount.'">'.$iCount.'</a> <li>';
                        }
                    }
                }
                // Render the target page number, but without it being a link
                $paginationCtrls .= '<li style="background-color: rgb(44, 44, 44) !important;"><a href="#">'.$iPageNum.'</a></li>';
                // Render clickable number links that should appear on the right of the target page number
                for($iCount = $iPageNum+1; $iCount <= $iLast; $iCount++){
                    $paginationCtrls .= '<li><a href="'.$_SERVER['PHP_SELF'].'?pn='.$iCount.'">'.$iCount.'</a> </li>';
                    if($iCount >= $iPageNum+4){
                        break;
                    }
                }
                // This does the same as above, only checking if we are on the last page, and then generating the "Next"
                if ($iPageNum != $iLast) {
                    $next = $iPageNum + 1;
                    $paginationCtrls .= '<li> <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'">Next</a> </li>';
                }
            }
            $list = '';
            ?>
            <html>
                <head>
                    <title>A ToDo Application</title>
                    <script src="js/jquery.js"></script>
                    <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
                    <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.min.css">
                    <link rel="stylesheet" href="css/bootstrap.min.css">
                    <script src="js/bootstrap.min.js"></script>
                    <script type="text/javascript" src="js/validation.js"></script>
                    <link rel="stylesheet" type="text/css" href="css/style.css">
                    <script>
                        $(function() {
                            $( "#datepicker" ).datepicker();
                        });
                    </script>
                </head>
                <body>
                    <div class="container classContainerBotPad">
                        <div class="classWelcome" id="idWelcome">
                            <?php include 'header.php'?>
                            <div class="classContent" id="idContent">
                                <div class="classArea" id="idArea">
                                        <h4 class="classH4">Completed Tasks</h4>

                                    <div class="classCompletedTable">
                                    <?php if($iRecordCount == 0){
                                            echo "<table class='table'><tr><th>There are no completed tasks.</th><tr></table>";
                                        }
                                        else{
                                    ?>
                                        <p><?php echo $vHeading; ?></p>
                                        <table class="table table-striped">
                                            <tr>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Due Date</th>
                                                <th>Priority</th>
                                            </tr>
                                            <tbody>
                                            <?php
                                            ////////////Fetching code for completed tasks///////////
                                            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
                                                $iid = $row["ID"];
                                                $sTitle = $row["Title"];
                                                $sDesc = $row["Description"];
                                                $dDuedate = $row["DueDate"];
                                                $sStatus = $row["Status"];
                                                $tiPri = $row["Priority"];
                                                if($tiPri == 0){
                                                    $tiPri = "High";
                                                }elseif($tiPri == 1){
                                                    $tiPri = "Normal";
                                                }else{
                                                    $tiPri = "Low";
                                                }
                                                $list .= '<tr><td>'.$sTitle.'</td><td>'.$sDesc.'</td><td>'.$dDuedate.'</td><td>'.$tiPri.'</td></tr>'
                                                ?>
                                                <tr>
                                    <?php
                                            }
                                        }
                                    ?>
                                                <?php echo $list; ?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    <div id="pagination_controls"><?php echo "<ul class='pagination'>".$paginationCtrls."</ul>"; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="classFooter" id="idFooter">
                            <h6>Copyright &copy; 2014-2015</h6>
                        </div>
                    </div>

                    <!-- Modal -->
                    <div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content classModalWidth">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Add a Task.</h4>
                                </div>
                                <form role="form" id="myForm" action="insertEntries.php" method="post" onsubmit="return validateTasks();">
                                    <div class="form-group">
                                        <div class="modal-body classModalForm">
                                            <?php include 'addTask.php'?>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <input type="submit" id="idBtn" class="btn btn-primary" value="Add Task">
                                        </div>
                                    </div>
                                </form>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->



                </body>
            </html>
<?php
        }
    }
    else{
        //echo "Session expired! Please login again.<br>";
        header("location: index.php");
    }
?>