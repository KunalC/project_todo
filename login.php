<!--<form role="form" name="frm" id="myForm" action="userLogin.php" method="post" onsubmit="return validateLogin();">-->
<form role="form" name="frm" id="myForm" action="userLogin.php" method="post">
    <div class="form-group">
          <div class="className" id="idUName">
            <span><label for="username">Username</label></span>
            <span class="spanEle3" id="idUNameTxt"><input type="text" class="form-control" id="uname" name="uname" placeholder="Enter Username"></span>
          </div>
          <div class="className" id="idPas">
            <span class="spanEle" id="idPas"><label for="pass">Password</label></span>
            <span class="spanEle4" id="idPasTxt"><input type="password" class="form-control" id="pas" name="pass" placeholder="Enter Password"></span>
          </div>
    </div>
    <div class="className" id="idSub">
        <div>
            <span class="spanEle" id="idSub"><input class="btn btn-primary" type="submit" value="Login"></span>
            <span class="classRegLink">New User!</span>
            <span><a data-toggle="modal" href="register.php" class="btn btn-primary">Register</a></span>
        </div>
    </div>
</form>