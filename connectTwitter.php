<?php
    session_start();
    include 'functions.php';
    $session = sessionCheckout();
    $iUserId = $_SESSION['uid'];
    if ($session){
        $mysqli = connectDB();
        $aTwitterConnection = getTwitterConnection($iUserId);
        if($aTwitterConnection['aTwitterAccessToken'] == "" && $aTwitterConnection['aTwitterAccessTokenSecret'] == ""){

            require_once 'lib/twitter/codebird.php';

            \Codebird\Codebird::setConsumerKey('PY6GWiCe6jBX43ObfORtVg', 'aZUHHLhFax6HcKALJhh8S6hOuTpSYP3OPyrSAm5tU'); // static, see 'Using multiple Codebird instances'

            $aCodeBird = \Codebird\Codebird::getInstance();

            if (!isset($_SESSION['oauth_token'])) {
                // Getting the request token
                $aReply = $aCodeBird->oauth_requestToken(array('oauth_callback' => 'http://localhost/ToDoApp/connectTwitter.php'));
                //var_dump($aReply);
                // Storing the token in session
                $aCodeBird->setToken($aReply->oauth_token, $aReply->oauth_token_secret);
                //var_dump($aCodeBird);exit;
                $_SESSION['oauth_token'] = $aReply->oauth_token;
                $_SESSION['oauth_token_secret'] = $aReply->oauth_token_secret;
                $_SESSION['oauth_verify'] = true;

                // redirect to auth website
                $auth_url = $aCodeBird->oauth_authorize();
                header('Location: ' . $auth_url);
                die();
            }
            elseif (isset($_GET['oauth_verifier']) && isset($_SESSION['oauth_verify'])) {

                // verify the token
                $aCodeBird->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
                unset($_SESSION['oauth_verify']);

                // get the access token
                $aReply = $aCodeBird->oauth_accessToken(array('oauth_verifier' => $_GET['oauth_verifier']));

                // store the token (which is different from the request token!)
                $_SESSION['oauth_token'] = $aReply->oauth_token;
                $_SESSION['oauth_token_secret'] = $aReply->oauth_token_secret;

                $sAccessToken = $_SESSION['oauth_token'];
                $sAccessSecretToken = $_SESSION['oauth_token_secret'];

                //store access token and access_token_secret into database

                $sTokenQuery = "UPDATE tbl_users_todo SET TwitterAccessToken='$sAccessToken', TwitterAccessTokenSecret='$sAccessSecretToken' WHERE UserID='$iUserId'";
                $aResutTokenQuery = $mysqli->query($sTokenQuery);

                // assign access token on each page load
                $aCodeBird->setToken($_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

                //tweet on timeline
                //$aReply = $aCodeBird->statuses_update('status=Successfully connected to Twitter');

                $sMsgConnect = 1;
                // send to same URL, without oauth GET parameters
                header('Location: settings.php?linkMsgConnect='.$sMsgConnect.'');
                die();
            }
            else{
                unset($_SESSION['oauth_token']);
                header('location: connectTwitter.php');
            }
        }
        else{
            header('location: settings.php');
            //header('location:profile.php?errorConnect=1');
        }
    }
    else{
        //echo "Session expired! Please login again.<br>";
        header("location: index.php");
    }

