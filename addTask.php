
<div class="className" id="idTit">
    <span class="spanEle" id="idTitle"><label for="title">Title</label></span>
    <span class="spanEle1" id="idTitleTxt"><input class="form-control" id="idtxtTitle" type="text" name="title" placeholder="Enter ToDo Title"></span>
</div>
<div class="className" id="idDes">
    <span class="spanEle" id="idDesc"><label for="desc">Description</label></span>
    <span class="spanEle2" id="idDescTxt"><textarea class="form-control" id="idtxtArea" class="classtxtarea" name="desc" placeholder="Enter Description"></textarea></span>
</div>
<div class="className" id="idDdate">
    <span class="spanEle" id="idDue"><label for="duedate">Due Date</label></span>
    <span class="spanEle3" id="iddueTxt"><input type="text" class="form-control" id="datepicker" name="duedate" readonly></span>
</div>
<div class="className" id="idPri">
    <span class="spanEle" id="idPriority"><label for="priority">Priority</label></span>
    <span class="spanEle5" id="idEmailTxt">
        <select class="form-control" id="idtxtPriority" name="priority">
            <option value="" selected="selected">Select a Value</option>
            <option value="2">Low</option>
            <option value="1">Normal</option>
            <option value="0">High</option>
        </select>
    </span>
</div>
