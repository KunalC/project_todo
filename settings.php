<?php
    session_start();
    include 'functions.php';
    require 'lib/facebook-sdk/facebook.php';
    $session = sessionCheckout();
    if ($session){
        //Reading values from the form.
        $username = $_SESSION['username'];
        $_SESSION['url'] = "settings.php";
?>
    <html>
        <head>
            <title>A ToDo Application</title>
            <script src="js/jquery.js"></script>
            <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
            <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.min.css">
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <script type="text/javascript" src="js/validation.js"></script>
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <script src="js/bootstrap.min.js"></script>
            <script>
                $(function() {
                    $( "#datepicker" ).datepicker();
                });
            </script>
        </head>
        <body>
            <div class="container classContainerBotPad">
                <div class="classWelcome" id="idWelcome">
                    <?php include 'header.php'?>
                    <div class="classContent" id="idContent">
                        <div class="classArea" id="idArea">
                            <h4 class="classH4">Settings</h4><hr>
                            <?php
                                $facebook = new Facebook(array(
                                    'appId'  => '436952173070954',
                                    'secret' => 'b48b43f74db70f885869678fdd5a548c',
                                    'allowSignedRequest' => false
                                ));
                                $user = $facebook->getUser();
                                if($user){
                                    $params = array( 'next' => 'http://localhost/ToDoApp/settings.php' );
                                    $sLogoutUrl = $facebook->getLogoutUrl($params);
                                    //var_dump($sLogoutUrl);
                                }
                                // Checking for Facebook connection.
                                if ($user) {
                            ?>
                            <h5>If you do not want to post your completed task : <a href="<?php echo $sLogoutUrl; ?>"><img src="images/fb_logout.png"></a></h5>
                            <div class="classFB">
                                <span class="glyphicon glyphicon-user classGlyUser"></span>
                                <span class="classFBConnected"><h5>You are Connect to Facebook : </h5></span>
                                <span class="classFBImg"><img class="img-thumbnail classFBImgSize" src="https://graph.facebook.com/<?php echo $user; ?>/picture"></span>
                                <h5>You can tweet completed tasks on your Twitter Timeline : <a href="connectTwitter.php"><img class="classSocialConnectImg" src="images/twitter-connect.png"></a></h5>
                            </div>
                            <?php
                                }
                                // Checking for Twitter connection.
                                elseif(isset($_GET['linkMsgConnect'])){
                                        if($_GET['linkMsgConnect'] == 1){?>
                                            <h5>You can post completed tasks on your Facebook Wall &nbsp : <a href="accessToken.php"><img class="classSocialConnectImg" src="images/fb-connect-large.png"></a></h5>
                                            <span class="glyphicon glyphicon-user"></span>
                                            <h5>You are Connect to Twitter : </h5>
                                           <?php
                                            //echo 'You successfully added a task.';
                                        }
                                }
                                else{
                                    echo '<h5>You can post completed tasks on your Facebook Wall &nbsp : <a href="accessToken.php"><img class="classSocialConnectImg" src="images/fb-connect-large.png"></a></h5>';
                                    echo '<h5>You can tweet completed tasks on your Twitter Timeline : <a href="connectTwitter.php"><img class="classSocialConnectImg" src="images/twitter-connect.png"></a></h5>';

                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="classFooter" id="idFooter">
                    <h6>Copyright &copy; 2014-2015</h6>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content classModalWidth">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Add a Task.</h4>
                        </div>
                        <form role="form" id="myForm" action="insertEntries.php" method="post" onsubmit="return validateTasks();">
                            <div class="form-group">
                                <div class="modal-body classModalForm">
                                    <?php include 'addTask.php'?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" id="idBtn" class="btn btn-primary" value="Add Task">
                                </div>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </body>
    </html>
<?php
    }
    else{
        //echo "Session expired! Please login again.<br>";
        header("location: index.php");
    }
?>