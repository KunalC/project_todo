/*function validateLogin(){
    var textUname = document.getElementById('uname').value;
    var textPass = document.getElementById('pas').value;
    if(textUname == "" && textPass == ""){
        alert("All fields are mandetory.");
        return false;
    }
    if(textUname == "" || textPass == "") {
        alert("All fields are mandetory.");
        document.getElementById('uname').focus();
        document.getElementById('uname').value = "";
        document.getElementById('pas').value = "";
        return false;
    }
    return true;
}*/

function validateRegister(){
    var textFn = document.getElementById('fn').value;
    var textLn = document.getElementById('ln').value;
    var textUn = document.getElementById('un').value;
    var textPs = document.getElementById('ps').value;
    var textEid = document.getElementById('eid');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(textFn == "" || textLn == "" || textUn == "" || textPs == "" || textEid == "") {
        alert("All fields are mandetory.");
        return false;
    }
    if(textFn == "" && textLn == "" && textUn == "" && textPs == "" && textEid == "") {
        alert("All fields are mandetory.");
        return false;
    }
    if (!filter.test(textEid.value)) {
        alert('Please enter a valid email address');
        textEid.focus();
        return false;
    }
    return true;
}

function validateTasks(){
    var textTitle = document.getElementById('idtxtTitle').value;
    var textDesc = document.getElementById('idtxtArea').value;
    var textDueDate = document.getElementById('datepicker');
    var textDue = textDueDate.value;
    var textPri = document.getElementById('idtxtPriority').value;

    //Split date entered by user
    var arr = textDue.split("/");
    var month = arr[0];
    var day = arr[1];
    var year = arr[2];

    //Get current date
    var todayDate = new Date();

    if(textTitle == "" || textDesc == "" || textDue == "" || textPri == "") {
        alert("All fields are mandetory.");
        return false;
    }
    if(textTitle == "" && textDesc == "" && textDue == "" && textPri == "") {
        alert("All fields are mandetory.");
        return false;
    }
    /*var dateformat = /^(?:(?:31\/(?:0[13578]|1[02])|(?:29|30)\/(?:0[13-9]|1[012])|(?:0[1-9]|1\d|2[0-8])\/(?:0[1-9]|1[0-2]))\/[2-9]\d{3}|29\/02\/(?:[2-9]\d(?:0[48]|[2468][048]|[13579][26])|(?:[2468][048]|[3579][26])00))$/;
    if (!(textDue).match(dateformat)) {
        alert("Date must be in DD/MM/YYYY format");
        textDueDate.focus();
        return false;
    }*/

    //Date comparison
    if(year >= todayDate.getFullYear()){
        if((month-1) >= todayDate.getMonth()){
            if(day >= todayDate.getDate()){
                return true;
            }
            else{
                alert("Entered Date must not be a past date");
                textDueDate.focus();
                return false;
            }

        }
        else{
            alert("Entered Date must not be a past date");
            textDueDate.focus();
            return false;
        }
    }
    else{
        alert("Entered Date must not be a past date");
        textDueDate.focus();
        return false;
    }
    return true;
}