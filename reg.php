
<form role="form" name="frm" id="myForm" action="userReg.php" method="post">
    <div class="form-group">
        <div class="className" id="idName">
            <span class="spanEle" id="idName"><label for="fname">First Name</label></span>
            <span class="spanEle1 classRegForm" id="idNameTxt"><input type="text" class="form-control" id="fn" name="fname" placeholder="Enter First Name"></span>
        </div>
        <div class="className" id="idName">
            <span class="spanEle" id="idLName"><label for="lname">Last Name</label></span>
            <span class="spanEle2 classRegForm" id="idLNameTxt"><input type="text" class="form-control" id="ln" name="lname" placeholder="Enter Last Name"></span>
        </div>
        <div class="className" id="idUName">
            <span class="spanEle" id="idUName"><label for="username">Username</label></span>
            <span class="spanEle3 classRegForm" id="idUNameTxt"><input type="text" class="form-control" id="un" name="uname" placeholder="Enter Userame"></span>
        </div>
        <div class="className" id="idPas">
            <span class="spanEle" id="idPas"><label for="pass">Password</label></span>
            <span class="spanEle4 classRegForm" id="idPasTxt"><input type="password" class="form-control" id="ps" name="pass" placeholder="Enter Password"></span>
        </div>
        <div class="className" id="idName">
            <span class="spanEle" id="idEmail"><label for="email">Email ID</label></span>
            <span class="spanEle5 classRegForm" id="idEmailTxt"><input type="text" class="form-control" id="eid" name="emailid" placeholder="Enter Email Address"></span>
        </div>
        <div class="classRegFormBtn" id="idSub">
            <span class="spanEle" id="idSub"><input class="btn btn-primary" type="button" value="Register" onclick="validateRegister();"></span>
        </div>
    </div>
</form>