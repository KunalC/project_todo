<?php
    session_start();
    include 'functions.php';
    $checkSession = sessionUrlChange();
    if($checkSession){
        header("location: " . $_SESSION['url']);
    }
    else{
?>
        <html>
            <head>
                <title>A ToDo Application</title>
                <script src="js/jquery.js"></script>
                <link rel="stylesheet" type="text/css" href="css/style.css">
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <script src="js/bootstrap.min.js"></script>
                <script type="text/javascript" src="js/validation.js"></script>
            </head>
            <body>
                <div class="container classContainerBotPad">
                    <div class="row">
                        <div class="col-lg-12" id="idWheader">
                            <center><h2>- ToDo Application -</h2></center>
                        </div>
                    </div>
                    <div class="row classContent">
                        <div class="col-lg-4 classLoginImage">
                            <img src="images/login.png">
                        </div>
                        <div class="classLoginAlert">
                            <?php
                                if(isset($_GET['linkError'])){
                                    if($_GET['linkError'] == 1){
                            ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                Incorrect username and password.
                                        </div>
                            <?php
                                    }
                                }
                                if(isset($_GET['linkReg'])){
                                    if($_GET['linkReg'] == 1){
                            ?>
                                        <div class="alert alert-success alert-dismissable">
                                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            User registered successfully.
                                        </div>
                            <?php
                                    }
                                }
                            ?>
                        </div>
                        <div class="col-lg-8 classLoginForm">
                            <div class="classAreaLogin" id="idAreaLogin">
                                <?php include 'login.php'; ?>
                            </div>
                        </div>
                    </div>
                    <div class="classFooter" id="idFooter">
                        <h6>Copyright &copy; 2014-2015</h6>
                    </div>
                </div>
            </body>
        </html>
<?php
    }
?>