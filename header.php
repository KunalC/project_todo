<div class="row">
    <div class="col-lg-12 classWheadFoot" id="idWheader">
        <center><h2>- ToDo Application -</h2></center>
    </div>
</div>
<div class="row">
    <div class="collapse navbar-collapse navbar-ex1-collapse classMenuBar">
        <ul class="nav navbar-nav">
            <li class="active"><a href="pendingTasks.php">Pending Task</a></li>
            <li><a href="#" data-toggle="modal" data-target="#addTaskModal">Add Task</a></li>
            <li><a href="completeShow.php">Completed Task</a></li>
            <li><a href="settings.php">Settings</a></li>
            <li><a href="userLogout.php">Logout</a></li>
        </ul>
        </div>
</div>