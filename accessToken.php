<?php
    session_start();
    require 'lib/facebook-sdk/facebook.php';
    include 'functions.php';
    $session = sessionCheckout();
    $userID = $_SESSION['uid'];
    if ($session){
        // Create our Application instance.
        $facebook = new Facebook(array(
            'appId'  => '436952173070954',
            'secret' => 'b48b43f74db70f885869678fdd5a548c',
            'allowSignedRequest' => false
        ));
        // Get User ID
        $user = $facebook->getUser();
        // We may or may not have this data based on whether the user is logged in.
        // If we have a $user id here, it means we know the user is logged into
        // Facebook, but we don't know if the access token is valid. An access
        // token is invalid if the user logged out of Facebook.

        //Checking if user exists
        if($user){
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                //$user_profile = $facebook->api('/me');
                // exit;
                $sToken = $facebook->getAccessToken();
                //Query for updating token in database.
                $mysqli = connectDB();
                if($mysqli){
                    $query = "UPDATE tbl_users_todo SET AccessToken = '$sToken' WHERE UserID = '$userID'";
                    $exe = $mysqli->query($query) ;
                    $msg = 1;
                    if(!$exe){
                        echo "Error occured. (" . $mysqli->errno . ")" . $mysqli->error;
                    }
                    else{
                        //echo '<script type="text/javascript">';
                        //echo 'alert("You are now connected to Facebook.");';
                        header("location: settings.php?link=".$msg."");
                        //echo '</script>';
                    }
                }
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = null;
            }
        }
        // Login or logout url will be needed depending on current user state.
        else {
            $loginUrl = $facebook->getLoginUrl();
            header("location:". $loginUrl);
        }
    }
    else{
        //echo "Session expired! Please login again.<br>";
        header("location: index.php");
    }
