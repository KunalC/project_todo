<?php
	session_start();
	include 'functions.php';
	$session = sessionCheckout();
	if ($session){
		//Reading values from the form.
		$username = $_SESSION['username'];
        $_SESSION['url'] = "addTaskForm.php";
?>
        <html>
            <head>
                <title>A ToDo Application</title>
                <link rel="stylesheet" type="text/css" href="css/style.css">
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <script src="js/bootstrap.min.js"></script>
                <script type="text/javascript" src="js/validation.js">
                </script>
            </head>
            <body>
                <div class="container classContainerBotPad">
                    <div class="classWelcome" id="idWelcome">
                        <?php include 'header.php'?>
                        <div class="classContent" id="idContent">
                            <div class="classArea">
                                <h4 class="classH4">Add Tasks to your ToDo List </h4>
                                <hr>
                                <div class="classAreaAdd" id="idArea">
                                    <?php include 'addTask.php'; ?>
                                </div>
                            </div>
                        </div>
                        <div class="classFooter" id="idFooter">
                                <h6>Copyright &copy; 2014-2015</h6>
                        </div>
                    </div>
                </div>
            </body>
        </html>
<?php
	}	
	else{
		header("location: index.php");
	}
?>