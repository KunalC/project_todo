<?php
	session_start();
	include 'functions.php';
	$session = sessionCheckout();
	if ($session){
		//Reading values from the form.
		$username = $session;
        $_SESSION['url'] = "welcome.php";

?>
<html>
	<head>
	  <title>A ToDo Application</title>
	  <link rel="stylesheet" type="text/css" href="css/style.css">
	  <script type="text/javascript" src="js/validation.js">
	  </script>
	</head>
	<body>		
	 	<div class="classWelcome" id="idWelcome">
	 		<div class="classWheader" id="idWheader">
	 			<center><h2>ToDo Application</h2></center>
	 		</div>
	 		<div class="classMenu" id="idMenu">
	 			<ul class="classMenuLst" id="idMenuLst">
	 				<li><a href="pendingTasks.php">Pending Task</a></li>
                    <li><a href="welcome.php">Add Task</a></li>
	 				<li><a href="completeShow.php">Completed Task</a></li>
                    <li><a href="settings.php">Settings</a></li>
                    <li><a href="userLogout.php">Logout</a></li>
	 			</ul>
	 		</div>	
	 		<div class="classContent" id="idContent">
	 			<div class="classArea" id="idArea">
	 				<div class="classProfile" id="idProfile">
	 					<!--<h3>Profile : <?php echo $session;?></h3>-->
	 				</div>
	 				<?php include 'addTask.php'; ?>
				</div>
		 	</div>
		 	<div class="classFooter" id="idFooter">
	 				<center><h4>Copyright &copy; 2014-2015</h4></center>
		 	</div>
	 	</div>
	</body>
</html>
<?php
	}	
	else{
		//echo "Session expired! Please login again.<br>";
		header("location: index.php");
	}
?>