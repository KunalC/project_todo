<?php
	include 'config/config.php';
    /* Function for Database Connection.
    ** This function will returns the database connection.
    */
	function connectDB(){
        $mysqli = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
        if ($mysqli->connect_errno) {
            return false;
		}
        else{
            return $mysqli;
        }
	}

	/* Function for Session check
	** This function will check the session and will return true if
	** session exists.
	*/
    function sessionCheckout(){
        if (isset($_SESSION['username'])){
            return true;
        }
    }

    /* Function for url misuse
    ** This function will check if anyone alters the URL when user logged in.
    */
    function sessionUrlChange(){
        if (isset($_SESSION['username'])){
            return true;
        }
    }

    /* Function for Logout
    ** This function logs out the user and destroys the session.
    */
	function sessionLogout(){
		session_destroy();
	}

    //function to check if the connection with twitter is established or not
    function getTwitterConnection($iUID){
        $oConnection = connectDB();
        $aResult = array();
        $sTokenQuery = "SELECT TwitterAccessToken, TwitterAccessTokenSecret FROM tbl_users_todo WHERE UserID='$iUID'";
        $oResultCheckToken = $oConnection->query($sTokenQuery);

        while ($aFetchToken = $oResultCheckToken->fetch_assoc()) {
            $aResult['aTwitterAccessToken'] = $aFetchToken['TwitterAccessToken'];
            $aResult['aTwitterAccessTokenSecret'] = $aFetchToken['TwitterAccessTokenSecret'];
        }
        $oConnection->close();
        return $aResult;
    }
?>