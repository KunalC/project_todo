<?php
	session_start();
	include 'functions.php';
	$session = sessionCheckout();
	if ($session){
		//Reading values from the form.
		$username = $_SESSION['username'];
        $_SESSION['url'] = "pendingTasks.php";
        $iUid = $_SESSION['uid'];
        $sStatus = "Pending";
        $mysqli = connectDB();
		if($mysqli){
			//Retrieving id of the user from database.
			$result = $mysqli->query("SELECT * FROM tbl_todo_entries WHERE UserID = \"$iUid\" and Status = \"$sStatus\"");
			$rowCount = mysqli_num_rows($result);
            //var_dump($rowCount);
            if(!$result){
				echo "Error occurred. (" . $mysqli->errno . ")" . $mysqli->error;
			}
            ?>
    <html>
        <head>
            <title>A ToDo Application</title>
            <script src="js/jquery.js"></script>
            <script src="js/jquery-ui-1.10.4.custom.min.js"></script>
            <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.min.css">
            <link rel="stylesheet" type="text/css" href="css/style.css">
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <script src="js/bootstrap.min.js"></script>
            <script type="text/javascript" src="js/validation.js"></script>
            <script>
                $(function() {
                    $( "#datepicker" ).datepicker();
                });
            </script>
        </head>
        <body>
            <div class="container classContainerBotPad">
                <div class="classWelcome" id="idWelcome">
                    <?php include 'header.php'?>
                    <div class="classContent" id="idContent">
                        <div class="classArea" id="idArea">
                            <div class="classProfile" id="idProfile">
                                <span class="classContentHead"><h4 class="classH4">Pending Tasks</h4></span>
                                <div class="classalert">
                                    <?php
                                        if(isset($_GET['linkEntries'])){
                                            if($_GET['linkEntries'] == 1){?>
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    You successfully added a task.
                                                </div><?php
                                              //echo 'You successfully added a task.';
                                            }
                                        }
                                    ?>
                                </div>
                            </div>
                            <hr>
                            <div class="classTodoPendingImg">
                                <img src="images/todo_list.jpg">
                            </div>
                            <?php if($rowCount == 0){
                                    echo "<table><tr><th>There are no pending tasks.</th><tr></table>";
                            }
                            else{?>
                            <form name="frm" id="myForm" action="completeTasks.php" method="post">
                            <table class="table table-striped classTablePendingFormat">
                                <tr>
                                    <th class="classPendingTableHead1"></th>
                                    <th class="classPendingTableHead2">Title</th>
                                    <th class="classPendingTableHead3">Description</th>
                                    <th class="classPendingTableHead4">Due Date</th>
                                    <th class="classPendingTableHead5">Priority</th>
                                </tr>
                                <tbody>
                                <?php
                                    while ($row = $result->fetch_assoc()){
                                    $iid = $row['ID'];
                                    $sTitle = $row['Title'];
                                    $sDesc = $row['Description'];
                                    $dDuedate = $row['DueDate'];
                                    $sStatus = $row['Status'];
                                    $tiPri = $row['Priority'];
                                    if($tiPri == 0){
                                        $tiPri = "High";
                                    }elseif($tiPri == 1){
                                        $tiPri = "Normal";
                                    }else{
                                        $tiPri = "Low";
                                    }?>
                                <tr>
                                    <td><input type="checkbox" name="task[]" value="<?php echo $iid; ?>"></td>
                                    <td><?php echo $sTitle; ?></td>
                                    <td><?php echo $sDesc; ?></td>
                                    <td><?php echo $dDuedate; ?></td>
                                    <td><?php echo $tiPri; ?></td>
                            <?php   }?>
                                </tr>
                                </tbody>
                            </table>
                                <input type="hidden" value="1" name="check" >
                                <input class="btn btn-primary" type="submit" value="Complete">
                            </form>
                                <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="classFooter" id="idFooter">
                    <h6>Copyright &copy; 2014-2015</h6>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content classModalWidth">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Add a Task.</h4>
                        </div>
                        <form role="form" id="myForm" action="insertEntries.php" method="post" onsubmit="return validateTasks();">
                            <div class="form-group">
                                <div class="modal-body classModalForm">
                                    <?php include 'addTask.php'?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <input type="submit" id="idBtn" class="btn btn-primary" value="Add Task">
                                </div>
                            </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        </body>
    </html>
<?php
            /*if(isset($_GET['linkEntries'])){
                if($_GET['linkEntries'] == 1){
                    echo '<script type="text/javascript">';
                    echo 'alert("Task Added Successfully.");';
                    echo 'window.location="pendingTasks.php"';
                    echo '</script>';
                }
            }*/
        }
	}	
	else{
		//echo "Session expired! Please login again.<br>";
        header("location: index.php");
	}
?>