<?php
    session_start();
    include 'functions.php';
    $checkSession = sessionUrlChange();
    if($checkSession){
        header("location: " . $_SESSION['url']);
    }
    else{
?>
        <html>
            <head>
                <title>A ToDo Application</title>
                <link rel="stylesheet" type="text/css" href="css/style.css">
                <link rel="stylesheet" href="css/bootstrap.min.css">
                <script src="js/bootstrap.min.js"></script>
                <script type="text/javascript" src="js/validation.js"></script>
            </head>
            <body>
                <div class="container classContainerBotPad">
                    <div class="row">
                        <div class="col-lg-12" id="idWheader">
                            <center><h1>- Registration -</h1></center>
                        </div>
                    </div>
                    <div class="row classContent">
                        <div class="col-lg-8 classRegForm1">
                            <div class="classAreaReg" id="idAreaReg">
                                <?php include 'reg.php'; ?>
                            </div>
                        </div>
                    </div>
                    <div class="classFooter" id="idFooter">
                        <h6>Copyright &copy; 2014-2015</h6>
                    </div>
                </div>
            </body>
        </html>
<?php
    }
?>