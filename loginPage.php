<html>
	<head>
	  <title>A ToDo Application</title>
	  <link rel="stylesheet" type="text/css" href="css/style.css">
	  <script type="text/javascript">
		function validate(){
			var textUname = document.getElementById('uname').value;
			var textPass = document.getElementById('pas').value;
			if(textUname == "" || textPass == "") {
				alert("All fields are mandetory.");
			}
			else{
				document.getElementById("myForm").submit();
			}
		}
		</script>
	</head>
	<body>		
	 	<div class="classWelcome" id="idWelcome">
	 		<div class="classWheader" id="idWheader">
	 			<center><h2>ToDo Application</h2></center>
	 		</div>
	 		<div class="classMenu" id="idMenu">
	 			<ul class="classMenuLst" id="idMenuLst">
	 				<li><a href="index.php">Register</a></li>
	 				<li><a href="loginPage.php">LogIn</a></li>
	 			</ul>
	 		</div>	
	 		<div class="classContent" id="idContent">
	 			<div class="classArea" id="idArea">		
	 				<?php include 'login.php'; ?>
				</div>
		 	</div>
		 	<div class="classFooter" id="idFooter">
	 				<center><h4>Copyright &copy; 2014-2015</h4></center>
		 	</div>
	 	</div>
	</body>
</html>